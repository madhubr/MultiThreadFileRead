package nbos.ThreadDemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * schema definition for this script
 * ==============================================
 * CREATE DATABASE PRODUCT;
 * <p>
 * CREATE TABLE PRODUCT (ID VARCHAR(100), NAME VARCHAR(500), PRICE VARCHAR(10), IMAGE_URL VARCHAR(1000) );
 * <p>
 * <p>
 * Created by madhum on 9/7/17.
 */
public class ThreadPoolExecDemo {
    static int batchsize = 5000;
    static int maxthread = 5;

    public static void main(String[] args) throws Exception {

        if (args != null && args.length > 0 && args[0] != null)
            batchsize = Integer.valueOf(args[0]);

        if (args != null && args.length > 0 && args[1] != null)
            maxthread = Integer.valueOf(args[1]);

        //String fileName = "/work/hadoop/h1/data/retail_db/order_items/part-00000";
        String fileName = "/work/hadoop/h1/retail_db/products/part-00000";
        DataSource ds = new FileDataSource(fileName);
        Executor executor = new Executor(maxthread);

        StatsThread stats = new StatsThread();
        Thread statsThread = new Thread(stats);
        statsThread.start();
        Stats statsObj = Stats.get();

        List<String> batch = ds.fetch(batchsize);
        while (batch != null && batch.size() > 0) {
            executor.execute(new Task(batch));
            statsObj.addToTotalRecords(batch.size());
            batch = ds.fetch(batchsize);
        }
        executor.stop();
        stats.finished();
        statsThread.join();
    }

}

class DB {
    PreparedStatement stmt = null;
    static Connection connection = null;

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        try {
            Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/product", "root", "root");
            c.setAutoCommit(false);
            return c;
        } catch (SQLException s) {
            s.printStackTrace();
        }
        return null;
    }
}

class StatsThread implements Runnable {
    boolean finished = false;

    StatsThread() {
    }

    @Override
    public void run() {
        while (!finished) {
            printStatus();
        }
        printStatus(); // printing status before exit
    }

    private void printStatus() {
        String stats = "\r" + Stats.get().printStats();
        try {
            System.out.write(stats.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void finished() {
        finished = true;
    }
}

class Stats {
    private static Stats stats = new Stats();

    private Stats() {
    }

    public static Stats get() {
        return stats;
    }

    //total records given to threads
    private Long totalRecords = new Long(0);
    //records processed by threads
    private Long recordsProcessed = new Long(0);

    synchronized public Long addToTotalRecords(int numRecords) {
        totalRecords += numRecords;
        return totalRecords;
    }

    synchronized public Long addToRecordsProcessed(int numRecords) {
        recordsProcessed += numRecords;
        return recordsProcessed;
    }

    public String printStats() {
        StringBuilder builder = new StringBuilder("Status: ");
        if (totalRecords > 0) {
            BigDecimal percentageCompleted = new BigDecimal(recordsProcessed).multiply(new BigDecimal(100));
            percentageCompleted = percentageCompleted.divide(new BigDecimal(totalRecords), BigDecimal.ROUND_HALF_DOWN);
            percentageCompleted = percentageCompleted.setScale(0, BigDecimal.ROUND_HALF_DOWN);
            builder.append(percentageCompleted.toPlainString() + "%")
                    .append(" \t total reocords: ").append(totalRecords)
                    .append(" \t records completed: ").append(recordsProcessed);
        }
        return builder.toString();
    }
}

class Executor {
    int maxthreads;
    ThreadPoolExecutor executorService;

    public Executor(int maxthreads) {
        this.maxthreads = maxthreads;
        executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(maxthreads);
    }

    public void execute(Task task) throws Exception {
        // stop taking new tasks if existing threads are busy processing
        while (executorService.getActiveCount() >= maxthreads) {
            //System.out.println("waiting for free thread");
            Thread.sleep(1000); // wait a second to check availability
        }
        // put the taks into queue to process
        executorService.execute(task);
    }

    public void stop() throws InterruptedException {
//        System.out.println("Shutting down");
        executorService.shutdown(); // this will not take new batches to process. so call to execute() will not take the
        executorService.awaitTermination(100L, TimeUnit.SECONDS);
        while (!executorService.isTerminated()) {
            //   Thread.sleep(1000*5) ;// wait 5 seconds
        }
    }

}

interface DataSource {

    public List<String> fetch(int batchsize) throws Exception;
}

/**
 * This class deals with the source data file that returns file in a BufferedReader
 * and batch of reccords in a list
 */
class FileDataSource implements DataSource {
    BufferedReader reader;
    int linesRead;

    /**
     * Returns file with BufferedReader
     * @param fileName
     * @throws Exception
     */
    public FileDataSource(String fileName) throws Exception {
        reader = new BufferedReader(new FileReader(new File(fileName)));
    }

    /**
     * Returns batch of records
     */
    public List<String> fetch(int batchsize) throws IOException {
        List<String> batch = new ArrayList<>(batchsize);
        String line = reader.readLine();
        while (line != null) {
            batch.add(line);
            if (batch.size() < batchsize) {
                line = reader.readLine();
            } else {
                line = null;
            }
        }
        return batch;
    }
}

class Task implements Runnable {

    private List<String> lines;
    PreparedStatement st;
    Connection connection;

    public Task(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public void run() {
        try {
            beginBatch();

            for (String line : lines) {
                processEachLine(line);
            }

            endBatch(lines);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 1. gets the jdbc connection
     * 2. prepares preparedStatement
     */
    private void beginBatch() throws SQLException {
        connection = DB.getConnection();
        String sql = "INSERT INTO PRODUCT (ID, NAME, PRICE, IMAGE_URL) VALUES (?, ?, ?, ?)";
        st = connection.prepareStatement(sql);
    }

    private void processEachLine(String line) throws SQLException {
        String[] tokens = line.split(",");
        if (tokens.length < 6) {
            // ERROR data
            System.out.println("error " + line);
            return;
        }
        st.setString(1, tokens[0]);
        st.setString(2, tokens[2]);
        st.setString(3, tokens[4]);
        st.setString(4, tokens[5]);
        st.addBatch();
    }

    private void endBatch(List<String> lines) {
        try {
            st.executeBatch(); // insert the batch of records added
            connection.commit(); // commit the batch inserts
            //update the stats
            Stats stats = Stats.get();
            stats.addToRecordsProcessed(lines.size());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException e) {
            }
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
        //System.out.println("completed batch:" + completed);
    }
}


